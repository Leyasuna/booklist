$(document).ready(function() 
{
    const base_url = "https://billgatesbooks.wl.r.appspot.com";
    const url2018 = base_url + "/2018";
    const url2019 = base_url + "/2019";
    const url2020 = base_url + "/2020";

    $("#2018").on("click", function(e) 
    {
        $("table tr td").each(function() 
        {
            $("table").empty();
        });
        $.ajax({
            url: url2018,
            dataType: "json",

            success: function(data, buildTable, allData) 
            {
                var allData = data["2018"];
                $.fn.buildTable(data, allData);
            }
        });
    });

    $("#2019").on("click", function(e) 
    {
        $("table tr td").each(function() 
        {
            $("table").empty();
        });
        $.ajax({
            url: url2019,
            dataType: "json",

            success: function(data, buildTable, allData) 
            {
                var allData = data["2019"];
                $.fn.buildTable(data, allData);
            }
        });
    });

    $("#2020").on("click", function(e) 
    {
        $("table tr td").each(function() 
        {
            $("table").empty();
        });
        $.ajax({
            url: url2020,
            dataType: "json",

            success: function(data, buildTable, allData) 
            {
                var allData = data["2020"];
                $.fn.buildTable(data, allData);
            }
        });
    });

    $.fn.buildTable = function(data, allData) 
    {
        $("table").addClass("table table-responsive");

        var year = allData[0].year;
        $('#book-year').text(year);

        let tableHeader = "<thead><tr><th scope='col'>Title</th><th scope='col' class='author'>Author</th><th scope='col' class='abstract'>Abstract</th></thead>";
        $("table").append(tableHeader);

        for (i = 0; i < allData.length; i++) 
        {
            var str = "<tr><td><figure><figcaption>" + allData[i].title + "</figcaption><img src='" + allData[i].image + "'></figure></td><td class='author'>" + allData[i].author + "</td><td class='abstract text-justify'>" + allData[i].abstract + "</td></tr>";
            $("td, th").addClass("align-middle");

            $("table").append(str);
        }
    };
});